#
#   Main
#   ****
#   Main file that will consult all other modules.
#

# Imports
from modules.settings import *
from modules.fileHandler import *
from modules.CSVInfoHandler import *
from modules.CSVAngabenHandler import *
from modules.csvLoader import *
from modules.creations import *
import time

time_start = time.time()

# function main: TODO
def main():
    # Load csv(s)
    csv_info = loadCSV(csv_info_path)
    csv_angaben = loadCSV(csv_angaben_path)
    
    # Clean the csv_info from all un-needed clutter
    csv_info = removeClutter(csv_info)
    # Get the information out of the csv_info
    csv_info_searchInformation = csvInfo_getInformation(csv_info)

    # Create genmenu
    genmenuPath = concatPaths(fileHandler_mainPath, "genmenu.txt")
    createFile(genmenuPath)
    writeBaseLayoutOfGenmenu(csv_info_searchInformation)

    # Clean the csv_angaben. Remove " " from the right side of the array entries
    csv_angaben = removeClutter(csv_angaben, -1, False, True)
    # Remove the first line of the csv (Remove the titles)
    csv_angaben.pop(0)
    # Create folder-structure, create hmenues, copy materials, create index.htmls, ...
    csvAngaben_main(csv_angaben, fileHandler_mainPath, csv_info_searchInformation)

    # Create index.html(which is on the same level as genmenu.txt). Will also create the "kruemel.txt"
    createLastIndexHtml(csv_info_searchInformation)

    # Print time-stats
    if print_stats:
        time_end = time.time()
        time_needed = time_end - time_start
        print("Time needed (in seconds):" + str(time_needed))

if __name__ == "__main__":
    main()
