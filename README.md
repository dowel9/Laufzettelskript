# Laufzettelskript

Ein Skript welches die Ordnerstruktur und die dazugehörigen html-Dateien erstellt.

## Voraussetzungen

Das Skript sollte für Python 3.6.2 und neuer funktionieren (Gestestet bisher: 3.6.2, 3.9.1, 3.10.4). Für ältere Versionen müsste man testen, Python 2.X wird nicht tun.

## Verwendung

-   _main.py_ zusammen mit dem Ordner _modules_ in das Verzeichniss der Wahl schieben
-   Den _materialien_-Ordner in dieses Verzeichniss hinzufügen
-   Beide Seiten des Laufzettelskripts müssen in _csv_ umgewandelt werden
    -   Dies muss für beide Seiten seperat geschehen
    -   Die _csv_-Dateien sollten schlussendlich _info.csv_ und _angaben.csv_ heißen (default)
    -   Eine Anleitung zum exportieren von _.ods_ nach _.csv_ gibt es z.B. [hier](https://help.libreoffice.org/3.3/Calc/Importing_and_Exporting_CSV_Files)
        -   Der Export nach _.csv_ **muss nach utf-8 geschehen !**
-   Sonstige Einstellungen die man eventuell vornehmen möchte bitte in der _modules/settings.py_ anpassen
-   Wenn alle Schritte fertig sind die _main.py_ ausführen
