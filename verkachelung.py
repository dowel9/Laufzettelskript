#
#   Verkachelung
#   ****
#   Description of what the script does: TODO
#
# https://lehrerfortbildung-bw.de/lfb_server/styleguide/guide/seitenelemente/links/
# style stil

# Settings
fileType_kachel_whitelist = ["pdf", "pptx", "odp", "docx", "odt", "doc", "xls", "xlsx", "ods", "mp4", "mp3", "ogg", "odb", "mdb"] # Filestypes that are allowed to be in a Kachel or the zip
files_kachel_blacklist = [] # Files not allowed in a kachel
folders_blacklist = [".git", "modules", "zzz_loeschen", "team", "info_fb", "1_material_auto", "__pycache__", "2_ispring"] # Folders we don't bother looking into
kachel_maxfiles = 6 # Maximum amount of files in a Kachel before we create a zip
kachel_sameFileNameCountAsOne = True # If we have abc.pdf and abc.docx they count as 1 file instead of two if set to True
kachel_randomPictures = ["./kachel1.jpg", "./kachel2.jpg", "./kachel3.jpg", "kachel4.jpg", "kachel5.jpg", "kachel6.jpg"] # The random picture in the Kachel and the path to it
zip_temporaryFolderName = "TMP_verkachelung_tmp_folder_for_zipping" # Make sure that this is a non-existant foldername otherwise everything in here is gone
zip_overwriteExistingZipFileWithSameName = True # If set to True we delete the zip with the same name and then create a new one. If set to False we stop when there is a zip already (Default: True)
output_fileName = "index_verkachelung.html" # You can add a path here aswell if you wan (for example: "myFolder/index.html"). But going out of the folder this script is in (f.e. with "..") will not work (hopefully)
hmenu_rename = {"hmenu.txt": "hmenu.txt.hide", "hmenu2.txt": "hmenu2.txt.hide", "hmenu3.txt": "hmenu3.txt.hide", "hmenu4.txt": "hmenu4.txt.hide"}

#
#
#
#
# Logic part (don't change stuff below here if you don't know why)
#
#
#
#
import shutil
import random
from modules.settings import *
from modules.fileHandler import *
from modules.tools import *
from pathlib import PureWindowsPath

# Logic settings
kachel_pictureCounter = 0
files_whitelist = fileType_kachel_whitelist + ["txt", "html"]
kachel_closing = """<div class="clearfix"></div>"""
kachel_downloadLink = """
          <a href="{}">
            <button class="btn dl-button more-link"><span class="{}-download-link">{}</span></button>
          </a>
"""
kachel_single = """
<!-- Spalte 1 -->
<div class="col-sm-6">
  <!-- Download-Kachel Start -->
  <div class="boxContainer">
    <div class="bg-white box clearfix">
      <div class="col-xs-12 fullWidthImageContainer">
        <div class="row"> <a href="{}"><img src="{}" alt="aussagekräftiger alt-Text zum Bild" border="0" class="img-responsive" /></a> </div>
        <h4>{}</h4>
        <div class="dl-button-box pull-right">
          {}
        </div>
      </div>
    </div>
  </div>
  <!-- Download-Kachel Ende -->
</div>
"""
kachel_double = """
<!-- Breiter Kacheldownload start -->
<div class="boxContainer">
  <div class="teaserWithImageLeft bg-white box clearfix">
    <div class="teaserContainer">
      <div class="teaser row">
        <div class="col-xs-12">
          <div class="teaserImageContainer col-sm-4 col-xs-12">
            <div class="row"> <a href="{}" target="_blank"> <img src="{}" class="img-responsive center-block" alt="aussagekräftiger alt-Text zum Bild" /> </a> </div>
          </div>
          <div class="teaserTextContainer col-sm-8 col-xs-12">
            <h4>{}</h4>
            <p>Hier ein Text, welcher neben dem Bild erscheint und eine kurze Beschreibung des Downloads enthält.</p>
            <div class="dl-button-box pull-right">
              {}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Breiter Kacheldownload ende -->
"""
kachel_index_ifNotAvailable = """
<html>
    <head>
        <title>NO INDEX.HTML FOUND TO INSERT BODY INTO. TAKING BACKUP.</title>
    </head>
    <body>
        {}
    </body>
</html>
"""
class Layout:
    def __init__(self, path) -> None:
        # Create
        self.path = path
        self.numberOfFiles = 0
        self.files = []
        self.kachelFiles = []
        self.fileBaseNames = []

        # Just to make sure our program is still within its constraints and just adjusting subfolders
        validateFileLocation(self.path)

        self.recur(self.path, 1)
    
    def __str__(self) -> str:
        fileNames = []
        for file in self.kachelFiles:
            fileNames.append(file.downloadLink)
        return "Layout with the path '" + str(self.path) + "' has a total of '" + str(self.numberOfFiles) + "' files. \n The files: " + str(fileNames) + "\n\n"

    def recur(self, currentPath: str, depth: int) -> None:
        files, folders = getFilteredFilesAndFolders(currentPath)
        # We add every file
        for file in files:
            filePath = concatPaths(currentPath, file)
            self.addFile(filePath)

        # And then go into every folder to do the same thing until there are no more folders left (and we are done)
        for folder in folders:
            folderPath = concatPaths(currentPath, folder)
            self.recur(folderPath, depth + 1)
    
    def addFile(self, filePath: str) -> None:
        file = File(filePath)
        
        # Always add it to the list of all files and if it is a file that should go to a kachel also add it
        self.files.append(file)
        if file.extension in fileType_kachel_whitelist:
            self.kachelFiles.append(file)
            if kachel_sameFileNameCountAsOne and file.baseName in self.fileBaseNames:
                self.numberOfFiles -= 1
            self.numberOfFiles += 1
            self.fileBaseNames.append(file.baseName)

class File:
    def __init__(self, path) -> None:
        self.path = path
        self.extension = getFileExtension(self.path)
        self.name = getFileBaseName(self.path)
        self.baseName = self.name.replace("." + self.extension, "")
        self.pathTo = self.path.replace(fileHandler_mainPath, "").replace(self.name, "")
        self.size = getFileSize(self.path)
        self.sizeBeautiful = byteSizeToKBMBGBTB(self.size)
        self.downloadLink = "." + PureWindowsPath(self.path.replace(fileHandler_mainPath, "")).as_posix()
    
    def __str__(self) -> str:
        return "The file '" + self.baseName + "' is a '" + self.extension + "' and is about '" + self.sizeBeautiful + "' large."
    
    def getDownloadName(self) -> str:
        # TODO: Just in case wenn index.html nicht findet den Dateinamen
        corrIndex = concatPaths(self.path.replace(self.name, ""), "index.html")
        found = False
        result = ""
        # Go through the file line by line. If we find the name ('xyz.pdf') in one line (the download-line) we know that in the following one theres a description
        with open(corrIndex, mode="r", encoding="utf-8") as f:
            lines = f.readlines()
            for line in lines:
                if found:
                    result = line.strip()
                    break
                if line.strip().find(self.name) != -1:
                    found = True
        if result == "":
            result = self.baseName
        return result

class Kachel:
    def __init__(self, files: list, closing: bool, single: bool = True) -> None:
        self.files = files
        self.single = single
        self.closing = closing # Will get ignored if single=False since we always add closing to double
        if not self.single:
            self.closing = True
        self.downloadTitel = self.files[0].getDownloadName()
        self.randomPicture = random.choice(kachel_randomPictures)
        #kachel_pictureCounter += 1

        # Create the html
        downloadList = ""
        for file in self.files:
            formattedText = kachel_downloadLink.format(file.downloadLink, file.extension, file.extension)
            downloadList += formattedText
        if self.single:
            self.html = kachel_single.format(self.files[0].downloadLink, self.randomPicture, self.downloadTitel, downloadList)
            if self.closing:
                self.html += kachel_closing
        else:
            self.html = kachel_double.format(self.files[0].downloadLink, self.randomPicture, self.downloadTitel, downloadList)
            self.html += kachel_closing

    def __str__(self) -> str:
        #return self.html
        return "This Kachel has is a single: '" + str(self.single) + " and has a total of '" + str(len(self.files)) + "' files. The download-title is: '" + self.downloadTitel + "'."

def main():
    files, folders = getFilteredFilesAndFolders(fileHandler_mainPath)
    # Check if we are in a (sub-)directory that seems to be generated by Laufzettelscript.py or something similar to make sure we don't run this everywhere
    # This means this script will only execute if there is an index.html in the directory of execution ! Pay attention to that.
    if "index.html" not in files:
        raise RuntimeError("No genmenu.txt found in the current directory ! Stopping script.")
    # If we find any hmenus we rename them
    for file in files:
        if file in hmenu_rename:
            fileName = concatPaths(fileHandler_mainPath, file)
            newFileName = concatPaths(fileHandler_mainPath, hmenu_rename[file])
            renameFile(fileName, newFileName)

    # We now create our Layout to count and sort the files (Our layout has a list of all files, where they are and which ones are important)
    #test = concatPaths(fileHandler_mainPath, "3_sprechen")
    layout = Layout(fileHandler_mainPath)

    # Create the kacheln
    kachelDone = []
    if layout.numberOfFiles <= kachel_maxfiles:
        kachelArray = getKachelArrays(layout)
        shouldBeSingle = True
        # If the number is not dividable by 2 we create one big Kachel
        if len(kachelArray) % 2 != 0:
            shouldBeSingle = False
        # We need a closing-thingy every second element
        closing = False
        for kachel in kachelArray:
            kachelDone.append(Kachel(kachel, closing, shouldBeSingle))
            closing = not kachelDone[-1].closing
            if not shouldBeSingle:
                shouldBeSingle = True
    else:
        # We create a tmp directory in which we then create the folder-structure with the files
        basePath = concatPaths(fileHandler_mainPath, zip_temporaryFolderName)
        createDirectory(basePath)
        for file in layout.kachelFiles:
            pathToFile = basePath + file.pathTo
            createDirectory(pathToFile)
            copyFileFromTo(file.path, pathToFile)
        
        # We zip all of that
        zipname = fileHandler_mainPath.replace(parentPath(fileHandler_mainPath), "")[1:]
        zippath = concatPaths(fileHandler_mainPath, zipname + ".zip")
        if doesFileExist(zippath):
            if not zip_overwriteExistingZipFileWithSameName:
                raise Exception("zip-file is already present and overwrite is set to False. Aborting")
            deleteFile(zippath)
        shutil.make_archive(zipname, "zip", basePath)
        zipfile = File(zippath)

        # And delete the tmp folder with all its contents again
        wipeDirectory(basePath)

        # Create the Kachel for this zip-download
        kachelDone.append(Kachel([zipfile], False, False))

    # Create the html of the page
    html = ""
    for kachel in kachelDone:
        html += kachel.html
    
    # Check our current output and we want to embed it into html
    if html == "":
        raise Exception("Whoops, something appears to have gone wrong. Your output is empty. Please check if the script is in a valid directory. If it should have worked then the mistake is in the Code :(")
    
    # Read out our availabe index.html, remove the body and replace it with ours. If the index.html does not have a body or the body-tag is not alone in a line we use our backup
    embed = ""
    bodyFound = False
    with open(concatPaths(fileHandler_mainPath, "index.html"), mode="r", encoding="utf-8") as f:
            lines = f.readlines()
            for line in lines:
                embed += line
                if line.strip() == "<body>":
                    bodyFound = True
                    break
    if bodyFound:
        html = embed + html + "\n  </body>\n</html>"
    else:
        html = kachel_index_ifNotAvailable.format(html)
        if print_general:
            print("The index.html did not have the body-tag in a single line. The script used the backup index.html !")

    # Write our file
    filePath = concatPaths(fileHandler_mainPath, output_fileName)
    writeIntoFile(filePath, html, "w", "utf8")

# function getKachelArrays: Expects a Layout-Object(object). Will create a list with lists inside. Each list inside should be a Kachel later
def getKachelArrays(layout: object) -> list:
    allKacheln = []
    removed = []
    for i in range(len(layout.kachelFiles)):
            if i in removed:
                continue
            kachelFiles = [layout.kachelFiles[i]]
            for n in range(len(layout.kachelFiles)):
                if n < i:
                    continue
                if layout.kachelFiles[n] == layout.kachelFiles[i]:
                    continue
                if layout.kachelFiles[n].baseName == layout.kachelFiles[i].baseName:
                    kachelFiles.append(layout.kachelFiles[n])
                    removed.append(n)
            allKacheln.append(kachelFiles)
    return allKacheln

# function getFilteredFilesAndFolders: Expects dir(string). Will return a filtered list of files and a filtered list of folders
def getFilteredFilesAndFolders(dir: str) -> tuple:
    # Get all files&folders from this directory
    files, folders = listAllFoldersAndFiles(dir)

    # Filter the files by looking at the file-extension (.pdf for example) and only keep those in the whitelist
    filteredFiles = []
    for file in files:
        if file not in files_kachel_blacklist:
            extension = getFileExtension(file)
            if extension in files_whitelist:
                filteredFiles.append(file)

    # Filter the folders by throwing away every folder in the blacklist
    filteredFolders = []
    for folder in folders:
        if folder not in folders_blacklist:
            filteredFolders.append(folder)
    
    return filteredFiles, filteredFolders

# This is supposed to be executed as a script and not imported
if __name__ == "__main__":
    main()