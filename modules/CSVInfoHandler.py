#
#   CSV-Handler for info.csv
#   ************************
#   Handles the info.csv (default-name). Will get the needed information out of the file and return the important variables.
#

# Imports
from .settings import *
import re as regex

# function csvInfo_getInformation: Expects csv(array). Will search for the information deposited in settings. Will return informationList(array)
def csvInfo_getInformation(csv: list) -> list:
    # Check if types are okay
    if (type(csv) != list or type(csv_info_searchInformation) != list):
        raise Exception("csv or searchlist is not a list.")

    # Go through every line in the csv as long as csv_info_searchInformation.length > 0
    informationList = []
    for line in csv:
        if (len(csv_info_searchInformation) <= 0): break
        # For this line check every remaining entry in csv_info_searchInformation if this is searched
        for entry in csv_info_searchInformation:
            searchResult = regex.search(entry, line[0])
            # If we found it: Cut it out of csv_info_searchInformation and save it into informationList
            if (searchResult):
                appendThis = line[0]
                if (len(line) == 2): appendThis = line[1]
                elif (len(line) > 2): appendThis = line
                informationList.append([entry, appendThis])
                # Delete the entry from csv_info_searchInformation
                csv_info_searchInformation.remove(entry)
                break
    return informationList