#
#   Filehandler
#   ************************
#   Should handle everything regarding files and directories. Will read, write, delete and create them (and more).
#

# Imports
from ntpath import realpath
from shutil import copy2, rmtree
from pathlib import Path
import shutil
from .settings import *
from .tools import *
import os
import hashlib

# function createDirectory: Expects path(string), canExist(bool). Recursivly creates that directory-path. Returns nothing
def createDirectory(path: str, canExist: bool = True):
    # Validate the path
    validateFileLocation(path)

    # Create the directory
    if print_detailed:
        print("Creating new directory: " + path)
    os.makedirs(path, exist_ok=canExist)

# function deleteDirectory: Expects path(string). Recursivly deletes that directory-path. Returns nothing
def deleteDirectory(path: str):
    # Validate the location
    validateFileLocation(path)

    # Delete the directory if it exists
    if doesDirExist(path):
        if print_detailed:
            print("Deleting the directory: " + path)
        os.rmdir(path)

# function wipeDirectory: Expects path(str). Will recursivly remove everything in that path !
def wipeDirectory(path: str):
    # Validate the location
    validateFileLocation(path)

    # Delete the directory and all of its contents
    if doesDirExist(path):
        if print_detailed:
            print("Deleting the directory and all of its contents: " + path)
        shutil.rmtree(path)

# function createFile: Expects file(string), content(string - optional), enc(string). Will create the file and write content into it. Returns
def createFile(file: str, content: str = "", enc: str = "utf8"):
    # Validate the file-location
    validateFileLocation(file)

    # Create the file with specified content
    if print_detailed:
        print("Creating file: " + file)
    f = open(file, "w", encoding=enc)
    if content != "":
        writeIntoFile(file, content)

# function deleteFile: Expects file(string). Deletes file. Returns nothing
def deleteFile(file: str):
    # Validate the file-location
    validateFileLocation(file)

    # Delete the file if it exists
    if doesFileExist(file):
        if print_detailed:
            print("Deleting file: " + file)
        os.remove(file)

# function renameFile: Expects file(str), newFileName(str). Both need to be full paths to the file ! Renames the file
def renameFile(file:str, newFileName: str):
    # Validate the file-location
    validateFileLocation(file)
    validateFileLocation(newFileName)

    # Rename the file
    if doesFileExist(file):
        if print_detailed:
            print("Renaming file: " + file + " into " + newFileName)
        os.rename(file, newFileName)

# function writeIntoFile: Expects file(string), content(string), param("a" or "w"), enc(string). By default "a"ppends the content to file. Returns nothing
def writeIntoFile(file: str, content: str, param: str = "a", enc: str = "utf8"):
    # Validate the file-location
    validateFileLocation(file)

    # Write the content into the file. "a" for append and "w" overwrites. More parameters: Internet
    f = open(file, param, encoding=enc)
    f.write(content)
    f.close

# function writeIntoFile: Expects file(string), enc(string). Will overwrite content, leaving an empty file. Returns nothing
def clearFileContent(file: str, enc: str = "utf8"):
    # Validate the file-location
    validateFileLocation(file)

    # Write "" into the file
    writeIntoFile(file, "", "w", enc)

# function readFileContent: Expects file(string), enc(string). Will open and read the file. Returns the content of the file(string)
def readFileContent(file: str, enc: str = "utf8") -> str:
    # Validate the file-location
    validateFileLocation(file)

    # Read and return the contents of the file
    f = open(file, "r", encoding=enc)
    content = f.read()
    return content

# function doesFileExist: Expects file(string). Checks if this file exists. Returns exists(boolean)
def doesFileExist(file: str) -> bool:
    # Validate the file-location
    validateFileLocation(file)

    # Check if the file exists and return True/False
    exists = os.path.isfile(file)
    return exists

# function doesDirExist: Expects dir(string). Checks if this directory exists. Returns boolean
def doesDirExist(dir: str) -> bool:
    # Validate the location
    validateFileLocation(dir)

    # Check if the directory exists. Return True/False
    dir = Path(dir)
    if dir.is_dir():
        return True
    else:
        return False

# function listAllFoldersAndFiles: Expects dir(string). Will list all files and folders in that directory. Returns tuple(files, folders)
def listAllFoldersAndFiles(dir: str) -> tuple:
    # Validate the file-location
    validateFileLocation(dir)

    # Create two lists: One for files and one for folders
    files = []
    folders = []
    for (dirpath, dirnames, filenames) in os.walk(dir):
        files.extend(filenames)
        folders.extend(dirnames)
        break
    
    # Return both lists
    return files, folders

# function copyFileFromTo: Expects source(string), destination(string). Will copy the source to the destination. Returns nothing
def copyFileFromTo(source: str, destination: str):
    # Validate the locations
    validateFileLocation(destination)
    checkFileSize(source)

    # If the file or target does not exist
    if not (doesFileExist(source) and doesDirExist(destination)):
        raise Exception("Tried to copy the file '" + source + "' to the folder '" + destination + "'. One of these is non-existent.")
    
    # Copy the file to the target
    finalFileLoc = concatPaths(destination, getFileBaseName(source))
    if not doesFileExist(finalFileLoc):
        if print_detailed:
            print("Copying the file '" + source + "' to '" + destination + "'")
        copy2(source, destination)
    else:
        if print_detailed:
            print("The file '" + finalFileLoc + "' will not be copied another time since it already exists.")

# function getFileSize: Expects file(string). Will return the fileSize(int)
def getFileSize(file: str) -> int:
    # Validate the file-location
    validateFileLocation(file)

    fileSize = os.path.getsize(file)
    return fileSize

# function checkFileSize: Expects file(string), max(int). Will check if the file at location is smaller/equal to the max. Returns boolean or Exception
def checkFileSize(file: str, max: int = fileHandler_maximumFileSize) -> bool:
    # Check if the filesize is smaller than the maximum
    fileSize = getFileSize(file)
    if max < fileSize:
        raise Exception("The file '" + file + "' is bigger(" + str(fileSize) + "B) than the maximum that is allowed to be copied(" + str(max) + "B).")
    else:
        return True

# function getHash_md5: Expects file(string). Will calculate the md5-hash of that file. Returns finalHash(string)
def getHash_md5(file: str) -> str:
    # Validate the file-location and make sure the file is within the max. size
    validateFileLocation(file)
    checkFileSize(file)

    # Calc the hash. We split up the hash into smaller chunks so we don't use 2GB of RAM when reading a 2GB file
    md5 = hashlib.md5()
    with open(file, "rb") as f:
        while True:
            data = f.read(fileHandler_hashReadBuffer)
            if not data:
                break
            md5.update(data)

    finalHash = md5.hexdigest()
    return finalHash

# function getHash_sha256: Expects file(string). Will calculate the sha256-hash of that file. Returns finalHash(string)
def getHash_sha256(file: str) -> str:
    # Validate the file-location and make sure the file is within the max. size
    validateFileLocation(file)
    checkFileSize(file)

    # Start to calculate the hash
    sha256 = hashlib.sha256()
    with open(file, "rb") as f:
        while True:
            data = f.read(fileHandler_hashReadBuffer)
            if not data:
                break
            sha256.update(data)

    finalHash = sha256.hexdigest()
    return finalHash

# function getRealativePath: Expects path(string), start(string). Will create a relative path from start to path. Returns relPath(string)
def getRealativePath(path: str, start: str) -> str:
    relPath = os.path.relpath(path, start)
    return relPath

# function concatPaths: Expects path1(string), path2(string). Will concat those two paths to a new one. Returns newPath(string)
def concatPaths(path1: str, path2: str) -> str:
    # Do not check if the paths are okay since we might want to concat paths that have nothing to do with our local filesystem !
    # Example: server-side paths for the index.html or h/gmenu
    newPath = os.path.join(path1, path2)
    return newPath

# function parentPath: Expects path(string). Will assume its a (valid) path and return the parent of this path. Returns newPath(string)
def parentPath(path: str) -> str:
    # Do not check if the path is okay since we might want get the parent of a path that is not on our local filesystem
    # Example: server-side paths for the index.html or h/gmenu
    newPath = os.path.abspath(concatPaths(path, os.pardir))
    return newPath

# function getFileBaseName: Expects file(string). Will return the filename cutting out the path. Returns fileName(string)
def getFileBaseName(file: str) -> str:
    # Do not check if the file-location is valid since the given path might not be a local one
    fileName = os.path.basename(file)
    return fileName

# function getFileExtension: Expects file(string), cutTheDot(boolean). Will get you the file type (.pdf or .ppt for example). Will return extension(string)
def getFileExtension(file: str, cutTheDot: bool = True) -> str:
    # Get the extension by cutting out the filename and path
    filename, extension = os.path.splitext(file)
    if cutTheDot:
        extension = extension[1:]
    return extension

# function validateFileLocation: Expects file(string). Checks if this location is in the same folder as the main.py. Returns isOk(boolean)
def validateFileLocation(file: str) -> bool:
    if (type(file) != str):
        raise Exception("File is not a string and therefor cannot be checked if the location is okay.")
    
    isOk = True
    # Check if the file is in the same folder or in any subfolder of the main.py
    if fileHandler_allowOutside == False:
        pathLengthOfMain = len(fileHandler_mainPath)
        if (file[:pathLengthOfMain] != fileHandler_mainPath):
            isOk = False
            raise Exception("Tried to access the following path: '" + file + "'. Parts of this path may lead outside of '" + fileHandler_mainPath + "'. Yes this WILL crash if theres only a 'info.csv' since it looks at the complete path.")

    return isOk
