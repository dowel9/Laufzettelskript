#
#   csv-Reader
#   **********
#   Loads csv-files while checking them for basic attributes such as size and file-ending.
#

# Imports
from .settings import *
from .fileHandler import validateFileLocation, getFileSize
from csv import reader

# function loadCSV: Expects filePath(string), maxSizeOfCSV(integer). Loads the csv and returns it as an array
def loadCSV(filePath: str, maxSizeOfCSV: int = csv_maxSize) -> list:
    # If validation is okay: Open file. If not the validateCSV-function will raise an error
    if validateFileLocation(filePath):
        if (validateCSV(filePath, maxSizeOfCSV) == True):
            with open(filePath, 'r', encoding='utf8') as file:
                csv_reader = reader(file)
                csvList = list(csv_reader)
                return csvList

# function removeClutter: Expects csv(array), minArrLengthToBeValid(int), removeEmptyEntry(boolean), removeRightSpaces(boolean).
# Will remove empty entries ; Will cut out spaces at the end of strings. Will return a new csv(array)
def removeClutter(csv: list, minArrLengthToBeValid: int = 1, removeEmptyEntry: bool = True, removeRightSpaces: bool = True) -> list:
    # Check if types are okay
    if type(minArrLengthToBeValid) != int:
        raise Exception("minArrLengthToBeValid is not an integer.")
    if type(csv) != list:
        raise Exception("csv is not a list.")
    if (type(removeRightSpaces) != bool or type(removeEmptyEntry) != bool):
        raise Exception("removeEmptyEntry and removeRightSpaces have to be booleans !")

    newCSV = []
    # Go through every line in the csv and do: Remove empty entries and remove spaces at the end of strings
    for line in csv:
        newLine = []
        for entry in line:
            include = True
            
            # If we want to remove spaces at the end of strings: Remove them
            if removeRightSpaces:
                entry.rstrip()

            # If we want to remove empty entries: Remove them
            if removeEmptyEntry:
                if (entry == ""):
                    include = False

            # Add them to our new CSV as the new line
            if include:
                newLine.append(entry)

        # Only add this to the new array if the number of variables is valid
        if (len(newLine) > minArrLengthToBeValid - 1):
            newCSV.append(newLine)

    return newCSV

# function validateCSV: Expects filePath(string), maxSizeOfCSV(integer). Checks if the filePath leads to a csv and checks that csv isn't too big. Returns isValid(boolean)
def validateCSV(filePath: str, maxSizeOfCSV: int) -> bool:
    isValid = True

    if validateFileLocation(filePath):
        # Check if filePath is a string
        if(type(filePath) != str):
            isValid = False
            raise Exception("Tried to load csv but the filepath is not a string.")
        # Check if file ends on ".csv"
        if (filePath[-4:] != ".csv"):
            isValid = False
            raise Exception("Tried to load " + filePath + ". This string does not end on '.csv'.")
        # Check if file is below the maximum
        fileSize = getFileSize(filePath)
        if (fileSize > maxSizeOfCSV):
            isValid = False
            raise Exception("Tried to load '" + filePath + "'. This file is " + str(fileSize) + "B big while the maximum is set to " + str(maxSizeOfCSV) + "B.")
        
        return isValid
