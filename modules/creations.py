#
#   Creations
#   *********
#   Split functions and functionalities that found no place in any other python file.
#   Currently: Create the first layout of the genmenu
#              Create the index html(the one on the same level as the genmenu)
#

# Imports
from .settings import *
from .tools import *
from .fileHandler import *

# function createLastIndexHtml: Expects info(csvlist). Will create the last index.html (same level as genmenu) and consult genmenu to get the links needed. Returns nothing
def createLastIndexHtml(info: list):
    # Get information out of the csvInfoArray
    for part in info:
        if part[0] == "Lehrgangsnummer:":
            lehrgangsnummer = part[1]
        elif part[0] == "Lehrgangstitel:":
            lehrgangstitel = part[1]

    # Create the local variables
    pageTitle = lehrgangsnummer + ": " + lehrgangstitel
    linkList,metatags = getLinkListFromGenMenu()
    
    # Create the content of the index.html
    content = f"""
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="keywords" content="{metatags}"/>
            <title>{pageTitle}</title>
        </head>

        <body>
            {settings_infoToFortbildungLink}
            <h2>{lehrgangstitel}</h2>
            {linkList}
        </body>
    </html>
    """
    # Prettify
    content = prettifyHTML(content)

    # Write into file
    indexPath = concatPaths(fileHandler_mainPath, "index.html")
    createFile(indexPath, content)

    # Also create the "kruemel.txt" if wanted
    if settings_allowKruemelTxt:
        indexPath = concatPaths(fileHandler_mainPath, "kruemel.txt")
        kruemelContent = unicodeToHTMLEntities(pageTitle)
        createFile(indexPath, kruemelContent)

# function getLinkListFromGenMenu: Expects nothing. Will go through the genmenu and create a linklist used for the last index.html. Returns linkList(string),metaTags(string)
def getLinkListFromGenMenu() -> tuple:
    # Variables needed
    linkList = ""
    metaTags = ""
    linkLayout = '<p><a href="{}" class="intern">{}</a></p>'

    # Open genmenu and read content
    genmenu = concatPaths(fileHandler_mainPath, "genmenu.txt")
    content = readFileContent(genmenu).split("\n")

    # Remove the first 5 entries from the genmenu. We do not want to link to the first entry with 1=> since it just links to the index.html
    # The last 4 entries are "Alle Dateien herunterladen", "Informationen zur Fortbildung" etc and we also dont want to link those
    content = content[5:len(content)-4]

    # Go through every remaining entry (every entry is now a link we need to link to)
    for page in content:
        # Every line looks like "1=>titel=>/foldername/" this (about). We now need titel and foldername
        entry = page.split("=>")

        # Remove the first "/" from the foldername and set the titel
        foldername = entry[2][1:]
        titel = entry[1]
        
        # Format our link-layout and add this string to the result
        newLink = linkLayout.format(foldername, titel)
        linkList = linkList + newLink
        metaTags = metaTags + titel + ", "
    
    # Return our new array
    metaTags = metaTags[:len(metaTags)-2] # Remove last ", "
    return linkList, metaTags


# function writeBaseLayoutOfGenmenu: Expects info(csvlist). Will create the layout of the genmenu and fill in the first lines. Returns nothing
def writeBaseLayoutOfGenmenu(info: list):
    # Read out info out of our angaben/info csv
    for part in info:
        if part[0] == csv_info_searchInfo_link:
            # TODO: This is a quick solution to make it work with https and http since we have to remove the first part of the link (up to "u_").
            link = part[1]
            link = link.split(".de")[1]
        elif part[0] == "Lehrgangsnummer:":
            lehrgangsnummer = part[1]
        elif part[0] == "Lehrgangstitel:":
            lehrgangstitel = part[1]
    RZ = "R=>1\nZ=>/../"
    titel = "1=>" + lehrgangsnummer + ": " + lehrgangstitel + "=>/index.html"

    # Write it into the genmenu
    content = "#\n" + "# " + link + "\n" + RZ + "\n" + titel
    writeIntoFile(concatPaths(fileHandler_mainPath, "genmenu.txt"), content)
