#
#   Settings
#   **********
#   All variables that you can change.
#

# Imports
import os

# General stuff
settings_fileSizeAccuracy = 3 # The number of digits that "count". Example (default: 3) it will turn "123.456 MB" into "123 MB". With 2 it will turn into "120 MB"
settings_infoToFortbildungLink = '<a href="bp2016.htm"><img align="right" alt="BP 2016" height="39" src="/st_bp2016/bp_2016.jpg" width="47"/></a>'
settings_allowedFileTypesInHGMenu = ["html", "htm"]
settings_allowKruemelTxt = True

# File-Handling
folder_materials = "1_material_auto" # Folder in which the pdfs and so on are. Make sure that it is on the same layer as the main.py !
fileHandler_mainPath = os.path.abspath(os.getcwd()) # Keep this automatic. Only change if you know what you are doing
fileHandler_allowOutside = False # Enable or disable the check wheather a folder/file is being created outside the folder the main.py is in. Default: False
fileHandler_maximumFileSize = 1000000000 # Maximum filesize when copying a file to another location - in bytes. Default is 1000000000 (1GB)
fileHandler_hashReadBuffer = 65536 # Number of Bytes being read in at once when creating a hash for a file

# CSV (general)
csv_info_path = os.path.join(fileHandler_mainPath, "info.csv")
csv_angaben_path = os.path.join(fileHandler_mainPath, "angaben.csv")
csv_maxSize = 5000000 # In Bytes (3 zeros is kB, 6 zeros is mB)
csv_info_searchInfo_link = "https?:\/\/lehrerfortbildung-bw\.de\/.*"
csv_info_searchInformation = ["Lehrgangstitel:", "Lehrgangsnummer:", csv_info_searchInfo_link]

# CSV (specific to: angaben.csv)(remember: computers start at 0 :) )
csv_schwarzeUeberschrift_location = 0
csv_angaben_maxEbene = 4
csv_angaben_realSeitenTitel_location = 5
csv_angaben_seitenUnterTitel = 6
csv_angaben_folderName_location = 7
csv_angaben_materialList_location = 8
csv_angaben_altDownloadBezeichnung = 9
csv_angaben_materialEbenenOffset_location = 10
csv_angaben_urheberrechtsform_location = 13

csv_angaben_indexHTML_goToButtonSearchString = '<!--We are searching for this exact comment to replace it with the "Weiter zu"-button-->'
csv_angaben_hashFileName = "<filename>.hashes" # '<filename>' will be replaced by the filename
csv_angaben_hashFileContent = "MD5: \n<md5>\n\nSha256: \n<sha256>" # '<md5>' and '<sha256>' will be replaced by the md5/sha256 hashes

# Prints and console-logs
print_general = True # Do you want generic console.logs ? This includes small error prints !
print_detailed = False # Very detailed prints (Warning: Spam)
print_stats = True # After program is done, print stats
