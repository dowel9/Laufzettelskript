#
#   Toolbox
#   *******
#   Toolbox for a number of tools that are needed and have no specific place in the other files
#

# Imports
from .settings import *
from bs4 import BeautifulSoup
from bs4.dammit import EntitySubstitution
import re

# Set Variables
escaper = EntitySubstitution()

# Overwrite bs4 1-space-indent with a default of 4 indent
# https://stackoverflow.com/questions/15509397/custom-indent-width-for-beautifulsoup-prettify
orig_prettify = BeautifulSoup.prettify
r = re.compile(r'^(\s*)', re.MULTILINE)
def prettify(self, encoding=None, formatter="minimal", indent_width=4):
    return r.sub(r'\1' * indent_width, orig_prettify(self, encoding, formatter))
BeautifulSoup.prettify = prettify

# function unicodeToHTMLEntities: Expects text(string). Converts unicode to HTML entities.  For example '&' becomes '&amp;'. Returns escaped(string)
# https://stackoverflow.com/questions/701704/convert-html-entities-to-unicode-and-vice-versa
def unicodeToHTMLEntities(text: str) -> str:
    actualType = type(text)
    if actualType != str:
        raise Exception("Tried to convert a text but text turned out to be " + actualType + " instead of a string.")
    escaped = escaper.substitute_html(text)
    return escaped

# function HTMLEntitiesToUnicode: Expects text(string). Converts HTML entities to unicode.  For example '&amp;' becomes '&'. Returns text(string)
def HTMLEntitiesToUnicode(text: str) -> str:
    actualType = type(text)
    if actualType != str:
        raise Exception("Tried to convert a text but text turned out to be " + actualType + " instead of a string.")
    text = BeautifulSoup(text).text
    return text

# function truncate: Expects f(float), n(int). Truncates/pads a float f to n decimal places without rounding. Returns result(int)
def truncate(f: float, n: int) -> float:
    # Source: https://stackoverflow.com/questions/783897/how-to-truncate-float-values
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    result = '.'.join([i, (d+'0'*n)[:n]])
    return float(result)

# function removeEmptyEntryFromArray: Expects array(list). Will remove all empty entries from the array. Returns newArray(list)
def removeEmptyEntryFromArray(array: list) -> list:
    newArray = []
    for entry in array:
        if entry != "":
            newArray.append(entry)    
    return newArray

# function prettifyHTML: Expects text(string). Will beautify the text using the html parser of bs4. Returns prettyText(string)
def prettifyHTML(text: str) -> str:
    soup = BeautifulSoup(text, 'html.parser')
    prettyText = soup.prettify()
    return prettyText

# function removeRightSpacesFromString: Expects text(string). Will remove " " from the right side of it. Returns text(string)
def removeRightSpacesFromString(text: str) -> str:
    if type(text) != str:
        raise Exception("Expected string but got " + type(text))
    
    return text.rstrip()

# function removeLeftSpacesFromString: Expects text(string). Will remove " " from the left side of it. Returns text(string)
def removeLeftSpacesFromString(text: str) -> str:
    if type(text) != str:
        raise Exception("Expected string but got " + type(text))
    
    return text.lstrip()

# function cutDecimalPlaces: Expects number(float/int). Will round the number to the desired length. Returns result(float)
def cutToDecimalPlaces(number: float, accuracy: int = settings_fileSizeAccuracy) -> float:
    # Since description is bad: cutToDecimalPlaces(123.456, 3) = 123 and cutToDecimalPlaces(123.456, 2) = 120
    # Don't yet know why but I need to add 1 to make it work properly
    accuracy = accuracy + 1

    # Cut off, round etc
    digitsBeforeDecimal = len(str(int(number)))
    move = accuracy - digitsBeforeDecimal
    number = int(number * pow(10, move))
    number = number * pow(10, -move)
    result = float(str(number)[:accuracy])
    return result

# function byteSizeToKBMBGBTB: Expects size(int), accuracy(int). Will get the closest size (one of kb, mb, gb, tb) and make a string out of it. Returns output(string)
def byteSizeToKBMBGBTB(size: int, accuracy: int = settings_fileSizeAccuracy) -> str:
    if type(size) != int or type(accuracy) != int:
        raise Exception("Expected integer but got something else. Needed is an integer variable to convert it to kb/mb/gb/tb")

    # TODO: Make this in a way I am okay with looking at it
    sizeKB = size / 1024
    sizeMB = sizeKB / 1024
    sizeGB = sizeMB / 1024
    sizeTB = sizeGB / 1024

    if sizeTB >= 1:
        output = str(cutToDecimalPlaces(sizeTB, accuracy)) + " TB"
    elif sizeGB >= 1:
        output = str(cutToDecimalPlaces(sizeGB, accuracy)) + " GB"
    elif sizeMB >= 1:
        output = str(cutToDecimalPlaces(sizeMB, accuracy)) + " MB"
    elif sizeKB >= 1:
        output = str(cutToDecimalPlaces(sizeKB, accuracy)) + " KB"
    else:
        output = str(cutToDecimalPlaces(size, accuracy)) + " B"
    
    return output.replace(".", ",")
