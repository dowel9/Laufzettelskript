#
#   CSV-Handler for angaben.csv
#   ***************************
#   Handles the angaben.csv (default-name). Will do stuff
#

# Imports
from re import split
from .settings import *
from .fileHandler import *
from .tools import *

# Internal vars
class PreviousHTMLClass:
    def __init__(self, path):
        self.path = path
    def getPrevious(self):
        return self.path
    def setPrevious(self, new):
        self.path = new
previousHTML = PreviousHTMLClass("")

# function csvAngaben_main: Expects list(array - csv), path(string). Will call upon all smaller functions to create (a mess) the folders/txts. Returns nothing
def csvAngaben_main(list: list, path: str, csv_info: list):
    # Set internal variables
    currentPath = path
    previousEbene = 0

    # Look at one line after another and create wanted materials. We go from top->bottom
    for line in list:
        # Get the information from our current line
        this_schwarzeUeberschrift = line[csv_schwarzeUeberschrift_location]
        this_pageSubTitle = line[csv_angaben_seitenUnterTitel]
        this_ebeneLevel = discoverEbenenLevel(line)
        this_ebeneName = line[this_ebeneLevel]
        this_folderName = line[csv_angaben_folderName_location]
        this_realSeitentitel = getRealPageTitle(this_ebeneName, line[csv_angaben_realSeitenTitel_location])
        this_altDownloadName = line[csv_angaben_altDownloadBezeichnung]
        this_urheberrechtsform = line[csv_angaben_urheberrechtsform_location]
        this_materialList = line[csv_angaben_materialList_location]
        this_materialOffset = line[csv_angaben_materialEbenenOffset_location]

        # Adjusted Variables which see later use
        this_folderIsActuallyFile = getFileExtension(this_folderName) != ""
        currentPath = updateCurrentPath(this_ebeneLevel, previousEbene, currentPath)# Adjust the path so that we create the folders/files at the correct place
        folderPath = concatPaths(currentPath, this_folderName)# Our current path to where we will create all other files
        baseLink = getLFBLinkFromCSVSearchInfo(csv_info)# Get the link from our info.csv related information
        
        # TODO: This looks terrible right now
        # Create the folder at our currentPath
        if not this_folderIsActuallyFile:
            createDirectory(folderPath)
        # Handle everything with and around g/hmenus
        HGMenuHandler(this_ebeneLevel, this_ebeneName, this_folderName, currentPath, this_schwarzeUeberschrift)
        # Copy and paste the pdfs, mp4s, oggs, ... to the specified location
        if not this_folderIsActuallyFile:
            materialSpreader(folderPath, this_materialList, this_materialOffset)
        # Create the index.html (Includes: schwarze Überschrift, SeitenUNTERtitel)
        if not this_folderIsActuallyFile:
            createIndexHtml(this_realSeitentitel, folderPath, this_altDownloadName, this_materialList, this_materialOffset, baseLink, this_pageSubTitle)
        # Create the htLocalControl.txt if wanted
        if not this_folderIsActuallyFile:
            manageHTLocalControl(this_urheberrechtsform, folderPath)

        # If we have a black title we dont have a folder name. When then going into the next line, the concat below will not result in a path that has another "layer"
        # That results in us going back 1 layer too much for each black title we have. So we just add a temporary name to the folder so we can go 1 layer back in the next line
        if this_folderName == "":
                this_folderName = "deleteThisFolderIfYouSomehowSeeIt"

        # Update internal vars for next line
        previousEbene = this_ebeneLevel
        currentPath = concatPaths(currentPath, this_folderName)
    
    # Remove the comment ("Weiter-zu") from the last index.html
    lastIndexHtml = previousHTML.getPrevious()
    if getFileBaseName(lastIndexHtml) == "index.html":
        # Read the content and replace the long search-string with nothing
        content = readFileContent(lastIndexHtml)
        content = content.replace(csv_angaben_indexHTML_goToButtonSearchString, "")
        content = prettifyHTML(content)
        writeIntoFile(lastIndexHtml, content, "w", "utf8")

def manageHTLocalControl(htLocalControl: str, pathTo: str):
    # Clean up the string
    htLocalControl = htLocalControl.strip()
    # Do we have to create the file ?
    if htLocalControl != "":
        if htLocalControl != "htLocalControl.txt":
            if print_general:
                print("Invalid entry while searching for htLocalControl.txt. Error came into play with this path: '" + pathTo + "'.")
        else:
            # Create the file
            fileLocation = concatPaths(pathTo, ".htLocalControl.txt")
            content = "copyrightIcon:==:C\ncopyrightUrl:==:/impressum/copyright/urheberrecht.html"
            createFile(fileLocation, content)

# function getRealPageTitle: Expects ebenenName(string), realName(string). Returns the title that will be used in the html (string)
def getRealPageTitle(ebenenName: str, realName: str) -> str:
    realName = removeRightSpacesFromString(realName)
    ebenenName = removeRightSpacesFromString(ebenenName)
    if realName == "":
        return ebenenName
    else:
        if ebenenName == "":
            raise Exception("Can't set the title of the page since both options are empty spaces...")
        return realName

# function updateCurrentPath: Expects cLevel(int), pLevel(int), cPath(str). Will update the path to the new and correct one. Returns cPath(str)
def updateCurrentPath(cLevel: int, pLevel: int, cPath: int) -> str:
    # cLevel is the currentEbenenLevel, pLevel the previousEbenenLevel and cPath is the path we want to adjust
    if type(cLevel) != int or type(pLevel) != int or type(cPath) != str:
        raise Exception("One or multiple of ebenen-Level or the path where to create a new folder is not a string or integer.")

    # Look at the difference of the ebenenlevel. Go back that many folders and return the result
    if cLevel < pLevel:
        difference = abs(cLevel - pLevel) + 1
        for i in range(difference):
            cPath = parentPath(cPath)
    elif cLevel == pLevel:
        cPath = parentPath(cPath)
    
    return cPath

# function HGMenuHandler: Expects ebene_level(int), ebene_name(str), ebene_folderName(str), path(str). Will forward information, in the end a entry in h/gmenu is created. Returns nothing
def HGMenuHandler(ebene_level: int, ebene_name: str, ebene_folderName: str, path: str, sUeberschrift: str):
    # TODO: h/gmenu will get the content appended every time the script runs. Meaning the file is not correct anymore when running the script twice.
    # Maybe add a "this file was created this time the script ran" and it will reset the files that were not yet visited ? - Or just writeIntoFile(w) the first time
    if type(ebene_level) != int or type(ebene_name) != str or type(ebene_folderName) != str or type(path) != str or type(sUeberschrift) != str:
        raise Exception("One or multiple of the following types dont match: Ebene-Level = int ; Ebene-Name = str ; Ebene-folderName = str ; Path = str, sUeberschrift = str")

    # If this is an entry that belongs into the gmenu
    if ebene_level == 1:
        filePath, content = addEntryToGMenu(ebene_name, ebene_folderName)
    # Else this entry belongs into an hmenu
    elif ebene_level > 0 and ebene_level <= csv_angaben_maxEbene:
        filePath, content = addEntryToHMenu(ebene_level, ebene_name, ebene_folderName, path)
    else:
        raise Exception("The current ebenenlevel is not correct. We are in a ebene with the name " + ebene_name)
    
    # Handle "schwarze Überschriften" by checking if it has one and if yes cut out specific parts of the string and replace them
    if hasBlackTitle(sUeberschrift):
        splitContent = content.split("=>")
        if len(splitContent) != 2:
            raise Exception("The created content that goes into the index.html has had " + str(len(splitContent)) + " '=>' in it. Expected 2.")
        content = "<small>" + splitContent[0] + "</small>=>no@link\n"

    # Write the content we got from the handlers into the file provided by the handlers
    # TODO: Shorten this
    if not doesFileExist(filePath):
        createFile(filePath, content)
    else:
        writeIntoFile(filePath, content)
    
# function hasBlackTitle: Expects sUeberschrift(string). Will determine if theres an x (and only 1 x) in this string (if yes -> black title). Returns boolean
def hasBlackTitle(sUeberschrift: str) -> bool:
    # Clean up the string by removing " " from the start&end of it
    sUeberschrift = sUeberschrift.strip()

    # Check if it is a black title
    if sUeberschrift == "x" or sUeberschrift == "X":
        return True
    elif sUeberschrift == "":
        return False
    else:
        raise Exception("Theres a wrong symbol in the black title. Expected 'x' or 'X' but got '" + sUeberschrift + "'.")

# function addEntryToHMenu: Expects ebene_level(int), ebene_name(str), ebene_folderName(str), path(str).
# Will create and fill the hmenues and add entries into the genmenu. Returns hmenuPath(string), content(string)
def addEntryToHMenu(ebene_level: int, ebene_name: str, ebene_folderName: str, path: str) -> tuple:
    # Remove "ä,ö,..." and replace them with the correct string (&uuml; for example)
    ebene_name = unicodeToHTMLEntities(ebene_name)

    # Get the name of the file and the path to it, create it if needed
    hmenuName = getGHMenueName(ebene_level)
    if hmenuName != "":
        # Create the content of what we want to write into the hmenu
        hmenuPath = concatPaths(path, hmenuName)
        pathWithFolderName = concatPaths(path, ebene_folderName)

        # If we have a file instead of a folder then use this (REMEMBER: This is only for hmenus, for genmenu you have to adjust in the respective function, sry :/)
        lastCharOfLink = "/"
        extension = getFileExtension(ebene_folderName)
        if extension != "":
            # We have a file
            if extension in settings_allowedFileTypesInHGMenu:
                lastCharOfLink = ""
            else:
                raise Exception("Tried to link invalid filetype into h or gmenu ! The name of the folder/file of '" + ebene_name + "' is '" + ebene_folderName + "'.")

        # Create the content we will write into the hmenu
        content = ebene_name + "=>" + pathWithFolderName.replace(fileHandler_mainPath, "") + lastCharOfLink + "\n"

        # When used on windows the path will have \ in there. Replace them
        content = content.replace("\\", "/")

        return hmenuPath, content

# function addEntryToGMenu: Expects titel(string), folderName(sting). Will format inputs together and add them into the genmenu. Returns filePath(string), content(string)
def addEntryToGMenu(titel: str, folderName: str) -> tuple:
    if type(titel) != str or type(folderName) != str:
        raise Exception("Titel or folderName are not strings. Expected strings.")
    
    # Remove "ä,ö,..." and replace them with the correct string (&uuml; for example)
    titel = unicodeToHTMLEntities(titel)

    # If we have a file instead of a folder then use this (REMEMBER: This is only for genmenu, for hmenus you have to adjust in the respective function, sry :/)
    lastCharOfLink = "/"
    extension = getFileExtension(folderName)
    if extension != "":
            # We have a file
            if extension in settings_allowedFileTypesInHGMenu:
                lastCharOfLink = ""
            else:
                raise Exception("Tried to link invalid filetype into h or gmenu ! The name of the folder/file of '" + titel + "' is '" + folderName + "'.")

    # Create the entry in our gmenu in this way: "1=>titel=>folderPath"
    content = "\n1=>" + titel + "=>/" + folderName + lastCharOfLink
    
    # If titel is something like "Alle Dateien herunterladen" we want another line below it:
    whitelistedTitelForSpace = ["Alle Dateien herunterladen", "Alle Materialien herunterladen"]
    if titel in whitelistedTitelForSpace:
        content = content + "\n1=>&nbsp;=>no@link"

    filePath = concatPaths(fileHandler_mainPath, "genmenu.txt")

    return filePath, content

# function materialSpreader: Expects target(string), materialList(string). Will read out all materials and move them from material folder into target-folder. Returns nothing
def materialSpreader(target: str, materialList: str, materialOffset: str):
    # Remove empty spaces from string and create array with materials in it (they are split by "," in the string by default). Remove empty entries
    materialList = removeEmptyEntryFromArray(materialList.replace(" ", "").split(","))
    materialOffset = removeEmptyEntryFromArray(materialOffset.replace(" ", "").split(","))

    # Go through every file and copy it from the material folder into the target
    materialFolder = concatPaths(fileHandler_mainPath, folder_materials)
    # Go through every entry and copy/past the material (in materialList there cannot be an empty entry - see lines above)
    for i in range(len(materialList)):
        materialPath = concatPaths(materialFolder, materialList[i])
        if doesFileExist(materialPath):
            copyTarget = getMaterialTargetConsideringEbenenOffset(target, materialOffset, i)
            copyFileFromTo(materialPath, copyTarget)
            makeMaterialHashFile(materialPath, copyTarget)
        else:
            raise Exception("Tried to copy the file '" + materialPath + "' to the location '" + target + "' but the file does not exist !")

# function getMaterialTargetConsideringMaterialOffset: Expects target(string). Will consider the offset and based on that change the actual target the file will go to. Returns target(string)
def getMaterialTargetConsideringEbenenOffset(target: str, offsetList: list, offsetIndex: int) -> str:
    maxOffset = csv_angaben_maxEbene - 1
    if offsetIndex < len(offsetList):
        offset = int(offsetList[offsetIndex])
        if offset > maxOffset:
            raise Exception("Tried to offset Download link but the actual offset is too high: " + str(offset))
        for n in range(abs(offset)):
            target = parentPath(target)
    return target

# function makeMaterialHashFile: Expects materialPath(string), targetPath(string). Will create a file with the md5/sha256-hashes of the material. Returns nothing
def makeMaterialHashFile(materialPath: str, targetPath: str):
    # Get the materialName and create the hashFileNames
    materialName = getFileBaseName(materialPath)
    hashesFileName = csv_angaben_hashFileName
    hashesFileName = hashesFileName.replace("<filename>", materialName)
    hashesFilePath = concatPaths(targetPath, hashesFileName)

    # Check if the file alread exists (sometimes material gets copied twice)
    if doesFileExist(hashesFilePath):
        return
    
    # Create the file and write the hashes into it
    md5 = getHash_md5(materialPath)
    sha256 = getHash_sha256(materialPath)
    content = csv_angaben_hashFileContent
    content = content.replace("<md5>", md5).replace("<sha256>", sha256)
    createFile(hashesFilePath, content)

# function createIndexHtml: Expects title(str), indexPath(str), downloadNameList(str), materialList(str), materialOffset(str), baseLink(str), pageSubTitle(str).
# Will create the content of the index html and write it into the file itself. Will also write some content in the inedx.html that was created before. Returns nothing
def createIndexHtml(titel: str, indexPath: str, downloadNameList: str, materialList: str, materialOffset: str, baseLink: str, pageSubTitle: str):
    # Create the list of downloads (for every file we need 1 link) TODO: Put vars in subfunction
    downloadList = ""
    materialOffset = removeEmptyEntryFromArray(materialOffset.replace(" ", "").split(","))
    materialList = removeEmptyEntryFromArray(materialList.replace(" ", "").split(","))
    downloadNameList = removeEmptyEntryFromArray(downloadNameList.split(","))

    # Create the list of download-material if theres any
    if len(materialList) > 0 :
        downloadList = createDownloadListForIndex(downloadNameList, materialList, indexPath, materialOffset, baseLink)
    # Create the subtitle for the page
    pageSubTitle = managePageSubTitle(pageSubTitle)

    # Create the (semi-)final content that will be inserted into the html
    # The "Go to the next page" segment will be added by the next runthrough
    indexHTML = f"""
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>{titel}</title>
        </head>

        <body>
            {pageSubTitle}
            {downloadList}
            <p></p>
            {csv_angaben_indexHTML_goToButtonSearchString}
        </body>
    </html>
    """

    # Write the index.html to the correct position
    indexPath = concatPaths(indexPath, "index.html")
    createFile(indexPath, indexHTML)

    # We now consult the PREVIOUS index.html and write the "Weiter zu" in there (we prettify it in there too)
    consultPreviousHTML(indexPath, titel, baseLink)

# function managePageSubTitle: Expects pageSubTitle(string). Will return what will be filled into the index.html for the page subtitle. Returns pageTitle(string)
def managePageSubTitle(pageSubTitle: str) -> str:
    if type(pageSubTitle) != str:
        raise Exception("The page subtitle is not a string.")
    # Clean up the string (remove leading and ending " ")
    pageSubTitle = pageSubTitle.strip()

    # Return formatted subtitle
    if pageSubTitle != "":
        pageTitle = f"<h2>{pageSubTitle}</h2>"
        return pageTitle
    else:
        return ""

# function consultPreviousHTML: Expects currHTML(string), topicName(string), baseLink(string). Will write the link to currHTML into the previous one. Returns nothing
def consultPreviousHTML(currHTML: str, topicName: str, baseLink: str):
    prevHTML = previousHTML.getPrevious()

    # If there is a previous html we need to write the button into
    if prevHTML != "":
        # Create the relativePath (we remove index.html and \. We also cut off the first three chars (always "../") since thats one folder too far)
        relativePath = getRealativePath(currHTML, prevHTML)
        relativePath = relativePath.replace("index.html", "").replace("\\", "/")[3:]
        newHTMLElement = f'<div id="contentMainInnerNextPage"><p>Weiter zu <a class="intern" href="{relativePath}"> {topicName} </a></p></div>'

        # Read the content, add our "Weiter zu" link and write our new content into the same file (overwrite the old one)
        content = readFileContent(prevHTML)
        content = content.replace(csv_angaben_indexHTML_goToButtonSearchString, newHTMLElement)
        content = prettifyHTML(content)
        writeIntoFile(prevHTML, content, "w", "utf8")

    # The next time we go through this we need the previous to be the current html
    previousHTML.setPrevious(currHTML)

# function createDownloadListForIndex: Expects downloadNameList(str), materialList(list), indexPath(string), materialOffset(list), baseLink(string).
# Will create a list of p element for the download-links (1 for each). Returns nothing
def createDownloadListForIndex(downloadNameList: str, materialList: list, indexPath: str, materialOffset: list, baseLink: str):
    if len(downloadNameList) != len(materialList):
        if print_general:
            print("The file-list '" + str(downloadNameList) + "' does not have a name for all downloads.")
    
    # Create the preset we will fill in
    endresult = ""

    # Fill our preset going through every download-link
    for i in range(len(materialList)):
        materialName = materialList[i]
        # Check if the download has a name to it
        if i >= len(downloadNameList):
            downloadName = materialName
        else:
            downloadName = getRealPageTitle(materialName, downloadNameList[i])
        # With the name now create the download-link with the details of filesize and extension
        materialDownloadLink = concatPaths(getMaterialTargetConsideringEbenenOffset(indexPath, materialOffset, i), materialName)
        materialFileExtension = getFileExtension(materialDownloadLink)
        materialSize = byteSizeToKBMBGBTB(getFileSize(materialDownloadLink))

        # Finish the link by replacing the current path to the file with the path to the file on the server
        materialDownloadLink = getRealativePath(materialDownloadLink, indexPath).replace("\\", "/")
        newDownload = f'<p>{downloadName}: <a class="download" href="{materialDownloadLink}">Herunterladen</a> [{materialFileExtension}] [{materialSize}]</p>'
        endresult = endresult + "\n" + newDownload
    
    # Throw the endresult into a div containing all the download-links
    endresult = f'<div id="contentMainInnerDownloads">{endresult}</div>'

    # We cut off the first "\n" with "1:"
    return endresult

# function getLFBLinkFromCSVSearchInfo: Expects csv_info(list). Searches for the start of the link in settings. Returns full link(str)
def getLFBLinkFromCSVSearchInfo(csv_info: list) -> str:
    link = ""
    for part in csv_info:
        if part[0] == csv_info_searchInfo_link:
            link = part[1]
    
    if link == "":
        raise Exception("Could not find the link in csv_info to create exact paths towards files (download-related). Probably searching for the var csv_info_searchInfo_link ?")
    return link

# function discoverEbenenLevel: Expects list(array). Will get the ebenenlevel. Returns level(int)
def discoverEbenenLevel(list: list) -> int:
    # TODO: Add something in settings to be able to adjust this easily
    if list[1] != "":
        return 1
    elif list[2] != "":
        return 2
    elif list[3] != "":
        return 3
    elif list[4] != "":
        return 4
    else:
        raise Exception("Ebenenlevelerror")

# function getGHMenueName: Expects ebene(int). Decides on the txt name based on ebene. Returns name(string)
def getGHMenueName(ebene: int) -> str:
    # TODO: Write a settings entry to be able to add more ebenen later (and maybe not elifs...)
    name = ""
    if ebene == 2:
        name = "hmenu.txt"
    elif ebene == 3:
        name = "hmenu2.txt"
    elif ebene == 4:
        name = "hmenu3.txt"
    elif ebene == 1:
        name = ""
    else:
        raise Exception("Ebenenlevel not correct")
    return name
